---
title: 'Advocate participation'
date: 2018-12-06T09:29:16+10:00
background: ''
button: ''
buttonLink: ''
---

# Advocate participation

Act as an advocate for our participants, raise awareness of their research and fully attribute them for their contribution.
