---
title: 'Digital Biomedical Assets'
date: 2018-11-28T15:15:34+10:00
icon: 'features/noun_branding_1885335.svg'
draft: false
featured: true
weight: 1
heroHeading: 'Digital Biomedical Assets'
heroSubHeading: 'Unlocking the information, eliminating the barriers with infrastructure that will incentivise, identify and reward.'
heroBackground: 'services/service2.jpg'
---

Digital Biomedical Assets come in many different forms, but generally fall under three categoriessdasadsa sdsa dsad sad:

## SEQUENCE DATA

Unlocking the information encoded in sequence data and understanding how this information is expressed in living biological systems holds the key to the development of future medicines, therapies and cures.

## CLINICAL DATA

Eliminating the barriers to the exchange of Clinical Data by leveraging technology from the financial sector which satisfies regulators and meets the demands of stringent compliance regimes.

## ALGORITHMIC ASSETS

Creating an infrastructure that will incentivise, identify and reward the creation of new analytic techniques and ensure they are made available to our participants.