---
title: 'Home'
date: 2018-02-12T15:37:57+07:00
heroHeading: 'BiOMEX - THE GLOBAL EXCHANGE FOR DIGITAL BIOMEDICAL ASSETS'
heroSubHeading: 'Create services and infrastructure that allows user to transact with DBAs simply, quickly and securely. We identify and work with providers of DBAs to list their assets for either commercial exchange or to enable non-profit sharing with other exchange users.'
heroBackground: 'images/jason-blackeye-1191801-unsplash.jpg'
---
