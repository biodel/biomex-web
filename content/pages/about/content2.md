---
title: 'About Content 2'
date: 2018-12-06T09:29:16+10:00
background: ''
button: ''
---

## Identify & Transact

Identify and match producers and consumers of Digital Biomedical Assets in transactions that generate value for both parties.
