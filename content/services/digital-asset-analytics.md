---
title: 'Digital Asset Analytics'
date: 2018-11-18T12:33:46+10:00
icon: 'features/noun_The Process_1885341.svg'
draft: false
featured: true
weight: 1
heroHeading: 'Digital Asset Analytics'
heroSubHeading: 'Real-time analytics provided over a broad range of asset transaction metrics provides unparalleled Market intelligence and analytical information on Bioinformatics research and development activity.'
heroBackground: 'services/service1.jpg'
---

Incentivises, identifies and rewards individuals and institutions whose listed DBAs have made a significant contribution to the life sciences.

## BiOMEX Digital Asset Analytics

A unique feature of our platform is the ability to provide full attribution for DBA holders whether they are data curators, algorithm developers or independent research institutions and thus raise awareness of their work and provide a valuable source of funding.
