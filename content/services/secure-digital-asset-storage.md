---
title: 'Secure Digital Asset Storage'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-2.png'
draft: false
featured: true
weight: 1
heroHeading: 'Secure Digital Asset Storage'
heroSubHeading: 'Specialised in protecting digital biomedical assets'
heroBackground: 'services/service1.jpg'
---

We are in the business of and specialise in protecting digital biomedical assets and thus we adhere to the highest standards of Information and Cyber Security.

## The BiOMEX Platform is a built on Distributed Ledger Technology.

Our platform employs the latest technology to deliver robust, secure and scalable computation but most importantly of all it is designed to be simple and intuitive to use. From a technical perspective BiOMEX is a distributed, cloud-native, community driven Bioinformatics Exchange.

We are in the business of and specialise in, protecting digital biomedical assets and thus we adhere to the highest standards of Information and Cyber Security.

In order to guarantee the privacy and integrity of the assets deployed to the exchange, we adopt a zero trust model to all of our information technology and business processes.

We maintain clearly demarcated boundaries between listed assets and Biomex infrastructure. All information and data are encrypted, at rest and in transit.
