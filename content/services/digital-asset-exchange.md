---
title: 'Digital Asset Exchange'
date: 2018-11-18T12:33:46+10:00
icon: 'services/service-icon-1.png'
draft: false
featured: true
weight: 1
heroHeading: 'Digital Asset Exchange'
heroSubHeading: 'BiOMEX interchanges are distributed applications that can be installed locally on your laptop and accessed by APIs in exactly the same way as connecting to the BiOMEX Cloud.'
heroBackground: 'services/service1.jpg'
---

Integrate BiOMEX into your own workflows and existing platforms just like any other bioinformatics application or tool.

## BiOMEX interchanges

Every aspect of Biomex is exposed via APIs so you can even monitor resource consumption, manage access and control your listed assets using your own command line tools or Web Applications.

Our platform is designed with the High Availability and High Performance capabilities demanded by mission critical systems.

A combination of distributed architecture and cloud infrastructure creates and incredibly resilient platform that maximises service availability.
